var todoModel = require('./../models/todo');

var todosFactory = function ($filter) {
    this.todos = [
        {
            name: 'Finish the TODOs',
            id: 3,
        },
        {
            name: 'Gear up for Raids',
            id: 2
        },
        {
            name: 'Do your node.js homework',
            id: 1
        },
    ];
    
};

todosFactory.prototype._where = function (keyname, value) {
    var searchedModel = [];
    for (var i in this.todos) {
        if (this.todos[i][keyname] !== value) continue;

        searchedModel = this.todos[i];
        break;
    }

    return searchedModel;
};

todosFactory.prototype.getNextIndex = function () {
    for(var a=0;a<this.todos.length;a++){
        this.todos[a].id = a;
    }
    return this.todos.length;
};

todosFactory.prototype.addTodo = function (todoProps) {
    todoProps.id = this.getNextIndex();
    this.todos.push(new todoModel(todoProps));
};

todosFactory.prototype.removeTodo = function (todoId) {

    for (var a in this.todos) {
        if (this.todos[a].id !== todoId) continue;

        var index = this.todos.indexOf(this.todos[a]);
        this.todos.splice(index, 1);
        break;
    }
};

// this.completeTodo = function(todoId){
//     this.todos._where('id', todoId).set('completed', true);
// };

module.exports = todosFactory;



// for (var i = 0; i < a.length; i++) {
//     if(a[i]+1 !== a[i+1]){
//         a[]
//     }
// }