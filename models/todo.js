var todoModel = function(props){
    if (!props) {
        this.setDefaults();
        return;
    }
    
    for(var i in props){
        this.set(i, props[i]);
    }
    
};

todoModel.prototype.setDefaults = function () {
    this.name = 'Default- do something';
    this.description = 'Some desc';
    this.id = null;  
};

todoModel.prototype.set = function (prop, val) {
    this[prop] = val;
}; 

todoModel.prototype.get = function (prop) {
    return this[prop] || false; 
};


module.exports = todoModel;