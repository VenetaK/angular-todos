(function () {
    // var angular = require('./libs/angular.min');
    var todosFactory = require('./collections/todos');
    var todosController = require('./controllers/todosController');

    var app = angular.module('todosApp', []);
    
    app.service('todosModel', function ($filter) {
        return new todosFactory($filter);
    });

    app.controller('ListTodosController', ['todosModel', function (todosModel) {
        return new todosController(todosModel);
    }]);
    
    
    //directives
    app.directive('todosList', function(){
        return {
            restrict: 'E',
            templateUrl: './views/todos.html',
            controller: 'ListTodosController',
            controllerAs: 'todosCtrl'
        };
    });
    
    app.directive('createForm', function(){
        return {
            restrict: 'E',
            templateUrl: './views/createForm.html',
            controller: 'ListTodosController',
            controllerAs: 'todosCtrl'
        };
    });

})();