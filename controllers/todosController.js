var todosController = function (todosCollection) {
    this.resetTodoModelProps();
    
    this.todosCollection = todosCollection;
    this.todos = todosCollection.todos;    
};

todosController.prototype.resetTodoModelProps = function () {
    this.modelProps = {
        name: '',
        desc: ''
    };
};

todosController.prototype.removeTodo = function (todoId) {
    this.todosCollection.removeTodo(todoId);
};

todosController.prototype.addTodo = function () {
    this.todosCollection.addTodo(this.modelProps);
    this.resetTodoModelProps();
};

module.exports = todosController;

